package com.ldoublem.loadingviewlib;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.app.Context;

/**
 * Created by lumingmin on 16/6/20.
 */

public class LVLineWithText extends Component implements Component.DrawTask {

    private Paint mPaintBar;
    private Paint mPaintText;
    private float mWidth = 0f;
    private float mHigh = 0f;
    private int mVlaue = 0;

    private float mPadding = 5f;

    public LVLineWithText(Context context) {
        this(context, null);
    }

    public LVLineWithText(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVLineWithText(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initPaint();
    }

    private void initPaint() {
        mPaintBar = new Paint();
        mPaintBar.setAntiAlias(true);
        mPaintBar.setStyle(Paint.Style.FILL_STYLE);
        mPaintBar.setColor(Color.WHITE);
        mPaintBar.setTextSize(dip2px(10));
        mPaintBar.setStrokeWidth(dip2px(1));

        mPaintText = new Paint();
        mPaintText.setAntiAlias(true);
        mPaintText.setStyle(Paint.Style.FILL_STYLE);
        mPaintText.setColor(Color.WHITE);
        mPaintText.setTextSize(dip2px(10));
        mPaintText.setStrokeWidth(dip2px(1));
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mHigh = getHeight();
        mWidth = getWidth();
        String text = mVlaue + "%";
        float textlength = getFontlength(mPaintText, text);
        float texthigh = getFontHeight(mPaintText, text);
        if (mVlaue == 0) {
            canvas.drawText(mPaintText, text, mPadding, mHigh / 2 + texthigh / 2);
            canvas.drawLine(new Point(mPadding + textlength, mHigh / 2), new Point(mWidth - mPadding, mHigh / 2), mPaintBar);
        } else if (mVlaue >= 100) {
            canvas.drawText(mPaintText, text, mWidth - mPadding - textlength, mHigh / 2 + texthigh / 2);
            canvas.drawLine(new Point(mPadding, mHigh / 2), new Point(mWidth - mPadding - textlength, mHigh / 2), mPaintBar);
        } else {
            float w = mWidth - 2 * mPadding - textlength;
            canvas.drawLine(new Point(mPadding, mHigh / 2), new Point(mPadding + w * mVlaue / 100, mHigh / 2), mPaintBar);
            canvas.drawLine(new Point(mPadding + w * mVlaue / 100 + textlength, mHigh / 2), new Point(mWidth - mPadding, mHigh / 2), mPaintBar);
            canvas.drawText(mPaintText, text, mPadding + w * mVlaue / 100, mHigh / 2 + texthigh / 2);
        }
    }

    public void setTextColor(int color) {
        mPaintText.setColor(new Color(color));
        invalidate();
    }

    public void setViewColor(int color) {
        mPaintBar.setColor(new Color(color));
        invalidate();
    }


    private float getFontlength(Paint paint, String str) {
        Rect rect = paint.getTextBounds(str);
        return rect.getWidth();
    }

    private float getFontHeight(Paint paint, String str) {
        Rect rect = paint.getTextBounds(str);
        return rect.getHeight();

    }

    private int dip2px(float dpValue) {
        return (int) (dpValue * getContext().getResourceManager().getDeviceCapability().screenDensity / 160);
    }

    public void setValue(int value) {
        this.mVlaue = value;
        invalidate();
    }

}
