package com.ldoublem.loadingviewlib;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by lumingmin on 16/6/20.
 */

public class LVCircularCD extends Component implements Component.DrawTask {

    private Paint mPaint;
    private float mWidth = 0f;
    private float mPadding = 0f;
    private float radius;
    private boolean isLoading = false;
    private AnimatorValue mProgerssRotateAnim;
    RectFloat rectF = new RectFloat();
    RectFloat rectF2 = new RectFloat();

    public LVCircularCD(Context context) {
        this(context, null);
    }

    public LVCircularCD(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVCircularCD(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initPaint();
    }

    private void initPaint() {
        mPadding = 5;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(Color.WHITE);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (getWidth() > getHeight()) {
            mWidth = getHeight();
        } else {
            mWidth = getWidth();
        }

        canvas.rotate(360 * radius, component.getWidth() / 2, component.getHeight() / 2);
        canvas.save();
        mPaint.setStrokeWidth(2);
        canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 2 - mPadding, mPaint);
        mPaint.setStrokeWidth(3);
        canvas.drawCircle(mWidth / 2, mWidth / 2, mPadding, mPaint);

        mPaint.setStrokeWidth(2);
        rectF = new RectFloat(mWidth / 2 - mWidth / 3, mWidth / 2 - mWidth / 3,
                mWidth / 2 + mWidth / 3, mWidth / 2 + mWidth / 3);
        canvas.drawArc(rectF, new Arc(0, 80, false), mPaint);
        canvas.drawArc(rectF, new Arc(180, 80, false), mPaint);

        rectF2 = new RectFloat(mWidth / 2 - mWidth / 4, mWidth / 2 - mWidth / 4,
                mWidth / 2 + mWidth / 4, mWidth / 2 + mWidth / 4);
        canvas.drawArc(rectF2, new Arc(0, 80, false), mPaint);
        canvas.drawArc(rectF2, new Arc(180, 80, false), mPaint);

        canvas.restore();

    }

    public void setViewColor(int color) {
        mPaint.setColor(new Color(color));
        invalidate();
    }

    public void startAnim() {
        startAnim(1500);
    }

    public void startAnim(int time) {
        isLoading = true;
        if (mProgerssRotateAnim == null) {
            mProgerssRotateAnim = new AnimatorValue();
            mProgerssRotateAnim.setDuration(time);
            mProgerssRotateAnim.setCurveType(Animator.CurveType.LINEAR);
            mProgerssRotateAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float value) {
                    radius = value;
                    getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            invalidate();
                        }
                    });

                }
            });
            mProgerssRotateAnim.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {
                }

                @Override
                public void onCancel(Animator animator) {
                }

                @Override
                public void onEnd(Animator animator) {
                    if (isLoading) {
                        startAnim();
                    }
                }

                @Override
                public void onPause(Animator animator) {
                }

                @Override
                public void onResume(Animator animator) {
                }
            });
        }
        if (isLoading) {
            mProgerssRotateAnim.start();
        }
    }

    public void stopAnim() {
        if (isLoading) {
            if (mProgerssRotateAnim != null) {
                try {
                    mProgerssRotateAnim.stop();
                    mProgerssRotateAnim = null;
                } catch (Exception e) {
                    Logger.getLogger(LVCircularCD.class.getName()).log(Level.WARNING, e.getMessage());
                }
            }
            isLoading = false;
        }
    }

}
