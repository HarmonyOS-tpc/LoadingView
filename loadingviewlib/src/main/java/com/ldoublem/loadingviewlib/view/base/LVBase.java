package com.ldoublem.loadingviewlib.view.base;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Paint;
import ohos.agp.utils.Rect;
import ohos.app.Context;

/**
 * Created by lumingmin on 2016/12/2.
 */

public abstract class LVBase extends Component {

    public final static int RESTART = 1;
    public final static int REVERSE = 2;
    public AnimatorValue animatorValue;
    private boolean isBack = false;

    public LVBase(Context context) {
        this(context, null);
    }

    public LVBase(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVBase(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        InitPaint();
    }

    public void startAnim() {
        startAnim(500);
    }

    public void startAnim(int time) {
        stopAnim();
        startViewAnim(time);
    }

    private AnimatorValue startViewAnim(long time) {
        animatorValue = new AnimatorValue();
        animatorValue.setDuration(time);
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
        animatorValue.setLoopedCount(SetAnimRepeatCount());
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                if (SetAnimRepeatMode() == REVERSE) {
                    if (isBack) {
                        OnAnimationUpdate(1 - value);
                    } else {
                        OnAnimationUpdate(value);
                    }
                } else if (SetAnimRepeatMode() == RESTART) {
                    OnAnimationUpdate(value);
                } else {
                    OnAnimationUpdate(value);
                }
            }
        });
        animatorValue.setLoopedListener(new Animator.LoopedListener() {
            @Override
            public void onRepeat(Animator animator) {
                OnAnimationRepeat(animator);
                isBack = !isBack;
            }
        });
        if (!animatorValue.isRunning()) {
            AinmIsRunning();
            animatorValue.start();
        }
        return animatorValue;
    }

    public void stopAnim() {
        if (animatorValue != null) {
            animatorValue.setLoopedCount(0);
            animatorValue.cancel();
            animatorValue.end();
            if (OnStopAnim() == 0) {
                animatorValue.setLoopedCount(0);
                animatorValue.cancel();
                animatorValue.end();
            }

        }
    }

    protected abstract void InitPaint();

    protected abstract void OnAnimationUpdate(float value);

    protected abstract void OnAnimationRepeat(Animator animation);

    protected abstract int OnStopAnim();

    protected abstract int SetAnimRepeatMode();

    protected abstract int SetAnimRepeatCount();

    protected abstract void AinmIsRunning();

    protected int dip2px(float dpValue) {
        return (int) (dpValue * getContext().getResourceManager().getDeviceCapability().screenDensity / 160);
    }

    protected float getFontlength(Paint paint, String str) {
        Rect rect = paint.getTextBounds(str);
        return rect.getWidth();
    }

    protected float getFontHeight(Paint paint, String str) {
        Rect rect = paint.getTextBounds(str);
        return rect.getHeight();

    }

    protected float getFontHeight(Paint paint) {
        Paint.FontMetrics fm = paint.getFontMetrics();
        return fm.descent - fm.ascent;
    }
}
