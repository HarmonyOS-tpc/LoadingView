package com.ldoublem.loadingviewlib.view;

import com.ldoublem.loadingviewlib.view.base.LVBase;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Created by lumingmin on 16/6/27.
 */

public class LVBattery extends LVBase implements Component.DrawTask {

    private float mWidth = 0f;
    private float mHight = 0f;
    private float mPadding;
    private float mBodyCorner;
    private float mBatterySpace;
    private Paint mPaint, mPaintHead, mPaintValue;
    RectFloat rectFBody = null;
    RectFloat rectHead = null;
    private float mAnimatedValue = 0f;
    private boolean mShowNum = false;
    private BatteryOrientation mBatteryOrientation = BatteryOrientation.HORIZONTAL;

    public LVBattery(Context context) {
        this(context, null);
    }

    public LVBattery(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVBattery(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mPadding = dip2px(2);
        mBodyCorner = dip2px(1);
        mBatterySpace = dip2px(1);
    }

    public enum BatteryOrientation {
        VERTICAL, HORIZONTAL
    }

    @Override
    protected void InitPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(Color.WHITE);
        mPaintHead = new Paint();
        mPaintHead.setAntiAlias(true);
        mPaintHead.setStyle(Paint.Style.FILL_STYLE);
        mPaintHead.setColor(Color.WHITE);
        mPaintValue = new Paint();
        mPaintValue.setAntiAlias(true);
        mPaintValue.setStyle(Paint.Style.FILL_STYLE);
        mPaintValue.setColor(new Color(Color.rgb(67, 213, 81)));
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (getWidth() > getHeight()) {
            mWidth = getHeight();
            mHight = getHeight() * 0.8f;
        } else {
            mWidth = getWidth();
            mHight = getWidth() * 0.8f;
        }
        if (mBatteryOrientation == BatteryOrientation.VERTICAL) {
            canvas.rotate(270, mWidth / 2, mWidth / 2);
        } else {
            canvas.rotate(0, mWidth / 2, mWidth / 2);
        }
        drawHead(canvas);
        drawBody(canvas);
        drawValue(canvas);
        drawLogo(canvas);
    }

    private void drawHead(Canvas canvas) {
        float mHeadwidth = mHight / 6;
        rectHead = new RectFloat(
                mWidth - mPadding - mHeadwidth,
                mWidth / 2 - mHeadwidth / 2,
                mWidth - mPadding,
                mWidth / 2 + mHeadwidth / 2);
        canvas.drawArc(rectHead, new Arc(-70, 140, false), mPaintHead);
    }

    private void drawBody(Canvas canvas) {
        float mHeadwidth = mHight / 6;
        float x = (float) ((mHeadwidth / 2) * Math.cos(-70 * Math.PI / 180f));
        rectFBody = new RectFloat();
        rectFBody.top = mWidth / 2 - mHight / 4 + mPadding;
        rectFBody.bottom = mWidth / 2 + mHight / 4 - mPadding;
        rectFBody.left = mPadding;
        rectFBody.right = mWidth - mPadding - x - x - mBatterySpace;
        canvas.drawRoundRect(rectFBody, mBodyCorner, mBodyCorner, mPaint);
    }

    private void drawValue(Canvas canvas) {
        RectFloat rectFBatteryValueFill = new RectFloat();
        rectFBatteryValueFill.top = rectFBody.top + mBatterySpace;
        rectFBatteryValueFill.bottom = rectFBody.bottom - mBatterySpace;
        rectFBatteryValueFill.left = mPadding + mBatterySpace;
        rectFBatteryValueFill.right = rectFBody.right - mBatterySpace;
        RectFloat rectFBatteryValue = new RectFloat();
        rectFBatteryValue.top = rectFBatteryValueFill.top;
        rectFBatteryValue.bottom = rectFBatteryValueFill.bottom;
        rectFBatteryValue.left = rectFBatteryValueFill.left;
        rectFBatteryValue.right = rectFBatteryValueFill.right * mAnimatedValue;
        canvas.drawRoundRect(rectFBatteryValue, 1, 1, mPaintValue);
    }

    private void drawLogo(Canvas canvas) {
        mPaintHead.setTextSize((int) mHight / 6);
        if (!mShowNum) {
            Path path = new Path();
            path.moveTo(mWidth / 2 - mHight / 6, mWidth / 2 - dip2px(1.5f));
            path.lineTo(mWidth / 2 + dip2px(2f), mWidth / 2 + mHight / 12);
            path.lineTo(mWidth / 2 + dip2px(1f), mWidth / 2);
            path.close();
            canvas.drawPath(path, mPaintHead);
            Path path2 = new Path();
            path2.moveTo(mWidth / 2 - dip2px(2f), mWidth / 2 - mHight / 12);
            path2.lineTo(mWidth / 2 + mHight / 6, mWidth / 2 + dip2px(1.5f));
            path2.lineTo(mWidth / 2 - dip2px(1f), mWidth / 2);
            path2.close();
            canvas.drawPath(path2, mPaintHead);
        } else {
            String text = (int) (mAnimatedValue * 100) + "%";
            if (mBatteryOrientation == BatteryOrientation.VERTICAL) {
                Path p = new Path();
                p.moveTo(mWidth / 2, 0);
                p.lineTo(mWidth / 2, mWidth);
                canvas.drawTextOnPath(mPaintHead, text, p,
                        mWidth / 2 - getFontlength(mPaintHead, text) / 2,
                        mWidth / 2 - mHight / 2 - getFontHeight(mPaintHead, text) / 2);
            } else {
                canvas.drawText(mPaintHead, text,
                        mWidth / 2 - getFontlength(mPaintHead, text) / 2,
                        mWidth / 2 + getFontHeight(mPaintHead, text) / 2);
            }
        }
    }

    public void setViewColor(int color) {
        mPaint.setColor(new Color(color));
        mPaintHead.setColor(new Color(color));
        invalidate();
    }

    public void setCellColor(int color) {
        mPaintValue.setColor(new Color(color));
        invalidate();
    }

    public void setValue(int value) { // 0-100
        this.mAnimatedValue = value * 1.0f / 100;
        invalidate();
    }

    public void setShowNum(boolean show) {
        this.mShowNum = show;
        invalidate();
    }

    public void setBatteryOrientation(BatteryOrientation batteryOrientation) {
        this.mBatteryOrientation = batteryOrientation;
        invalidate();
    }

    @Override
    protected void OnAnimationRepeat(Animator animation) {

    }

    @Override
    protected void OnAnimationUpdate(float value) {
        mAnimatedValue = value;
        invalidate();
    }

    @Override
    protected int OnStopAnim() {
        mAnimatedValue = 0f;
        invalidate();
        return 1;
    }

    @Override
    protected int SetAnimRepeatMode() {
        return RESTART;
    }

    @Override
    protected void AinmIsRunning() {

    }

    @Override
    protected int SetAnimRepeatCount() {
        return Integer.MAX_VALUE;
    }

}
