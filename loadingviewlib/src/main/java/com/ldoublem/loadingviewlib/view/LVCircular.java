package com.ldoublem.loadingviewlib.view;

import com.ldoublem.loadingviewlib.view.base.LVBase;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Created by lumingmin on 16/6/20.
 */

public class LVCircular extends LVBase implements Component.DrawTask {

    private Paint mPaintCenter;
    private Paint mPaintRound;

    private float mWidth = 0f;
    private float mMaxRadius = 4;
    private int mStartAngle = 0;
    private float mAnimatedValue = 0f;

    public LVCircular(Context context) {
        this(context, null);
    }

    public LVCircular(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVCircular(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void InitPaint() {
        mPaintCenter = new Paint();
        mPaintCenter.setAntiAlias(true);
        mPaintCenter.setStyle(Paint.Style.FILL_STYLE);
        mPaintCenter.setColor(Color.WHITE);

        mPaintRound = new Paint();
        mPaintRound.setAntiAlias(true);
        mPaintRound.setStyle(Paint.Style.FILL_STYLE);
        mPaintRound.setColor(Color.WHITE);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (getWidth() > getHeight()) {
            mWidth = getHeight();
        } else {
            mWidth = getWidth();
        }
        mMaxRadius = mWidth / 30f;
        canvas.rotate(360 * mAnimatedValue, component.getWidth() / 2, component.getHeight() / 2);
        for (int i = 0; i < 9; i++) {
            float x2 = (float) ((mWidth / 2.f - mMaxRadius) * Math.cos(mStartAngle + 45f * i * Math.PI / 180f));
            float y2 = (float) ((mWidth / 2.f - mMaxRadius) * Math.sin(mStartAngle + 45f * i * Math.PI / 180f));
            canvas.drawCircle(mWidth / 2.f - x2, mWidth / 2.f - y2, mMaxRadius, mPaintRound);
        }
        canvas.drawCircle(mWidth / 2.f, mWidth / 2.f, mWidth / 2.f - mMaxRadius * 6, mPaintCenter);
    }


    public void setViewColor(int color) {
        mPaintCenter.setColor(new Color(color));
        invalidate();
    }

    public void setRoundColor(int color) {
        mPaintRound.setColor(new Color(color));
        invalidate();
    }

    @Override
    public void startAnim() {
        startAnim(3500);
    }

    @Override
    protected void OnAnimationUpdate(float value) {
        mAnimatedValue = value;
        invalidate();
    }

    @Override
    protected void OnAnimationRepeat(Animator animation) {

    }

    @Override
    protected int OnStopAnim() {
        return 0;
    }

    @Override
    protected int SetAnimRepeatMode() {
        return RESTART;
    }

    @Override
    protected void AinmIsRunning() {

    }

    @Override
    protected int SetAnimRepeatCount() {
        return Integer.MAX_VALUE;
    }
}
