package com.ldoublem.loadingviewlib.view;

import com.ldoublem.loadingviewlib.view.base.LVBase;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Created by lumingmin on 16/7/1.
 */

public class LVFunnyBar extends LVBase implements Component.DrawTask {

    private Paint mPaintLeftTop, mPaintLeftLeft, mPaintLeftRight;
    private Paint mPaintRightTop, mPaintRightLeft, mPaintRightRight;
    private int mWidth = 0;
    private int mHeight = 0;
    private float mAnimatedValue = 1f;

    public LVFunnyBar(Context context) {
        this(context, null);
    }

    public LVFunnyBar(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVFunnyBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void InitPaint() {
        mPaintLeftTop = new Paint();
        mPaintLeftTop.setAntiAlias(true);
        mPaintLeftTop.setStyle(Paint.Style.FILL_STYLE);
        mPaintLeftTop.setColor(new Color(Color.rgb(234, 167, 107)));
        mPaintLeftLeft = new Paint();
        mPaintLeftLeft.setAntiAlias(true);
        mPaintLeftLeft.setStyle(Paint.Style.FILL_STYLE);
        mPaintLeftLeft.setColor(new Color(Color.rgb(174, 113, 94)));
        mPaintLeftRight = new Paint();
        mPaintLeftRight.setAntiAlias(true);
        mPaintLeftRight.setStyle(Paint.Style.FILL_STYLE);
        mPaintLeftRight.setColor(new Color(Color.rgb(138, 97, 85)));
        mPaintRightTop = new Paint();
        mPaintRightTop.setAntiAlias(true);
        mPaintRightTop.setStyle(Paint.Style.FILL_STYLE);
        mPaintRightTop.setColor(new Color(Color.rgb(234, 167, 107)));
        mPaintRightLeft = new Paint();
        mPaintRightLeft.setAntiAlias(true);
        mPaintRightLeft.setStyle(Paint.Style.FILL_STYLE);
        mPaintRightLeft.setColor(new Color(Color.rgb(174, 113, 94)));
        mPaintRightRight = new Paint();
        mPaintRightRight.setAntiAlias(true);
        mPaintRightRight.setStyle(Paint.Style.FILL_STYLE);
        mPaintRightRight.setColor(new Color(Color.rgb(138, 97, 85)));
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mWidth = getWidth();
        mHeight = getHeight();
        mHeight = (int) (mWidth / Math.sqrt(3));
        int heightPadding = (mWidth - mHeight) / 2 > 0 ? (mWidth - mHeight) / 2 : 0;
        float wspace = mWidth / 8f;
        float hspace = mHeight / 8f;
        Path p = new Path();
        float leftLong;
        for (int i = 0; i < 3; i++) {
            leftLong = mAnimatedValue * (1 + i / 4f);
            if (leftLong > 1f) {
                leftLong = 1f;
            }
            float wlong = mWidth / 2 * leftLong - wspace / 2;
            float hlong = mHeight / 2 * leftLong - hspace / 2;
            if (wlong < wspace / 8 / 8 / 2) {
                wlong = wspace / 8 / 8 / 2;
            }
            if (hlong < hspace / 8 / 8 / 2) {
                hlong = wspace / 8 / 8 / 2;
            }
            p.reset();
            p.moveTo((i + 0.5f) * wspace, mHeight / 2 + (i * hspace) + heightPadding);
            p.lineTo((i + 1f) * wspace + wlong, mHeight / 2 - hspace / 2f + (i * hspace) - hlong + heightPadding);
            p.lineTo((i + 1.5f) * wspace + wlong, mHeight / 2 + (i * hspace) - hlong + heightPadding);
            p.lineTo((i + 1f) * wspace, mHeight / 2 + hspace / 2f + (i * hspace) + heightPadding);
            p.close();
            canvas.drawPath(p, mPaintLeftTop);
            p.reset();
            p.moveTo((i + 0.5f) * wspace, mHeight / 2 + (i * hspace) + heightPadding);
            p.lineTo((i + 1f) * wspace, mHeight / 2 + hspace / 2f + (i * hspace) + heightPadding);
            p.lineTo((i + 1f) * wspace, mHeight / 2 + hspace / 2f + (i * hspace) + hspace + heightPadding);
            p.lineTo((i + 0.5f) * wspace, mHeight / 2 + (i * hspace) + hspace + heightPadding);
            p.close();
            canvas.drawPath(p, mPaintLeftLeft);
            p.reset();
            p.moveTo((i + 1.5f) * wspace + wlong, mHeight / 2 + (i * hspace) - hlong + heightPadding);
            p.lineTo((i + 1f) * wspace, mHeight / 2 + hspace / 2f + (i * hspace) + heightPadding);
            p.lineTo((i + 1f) * wspace, mHeight / 2 + hspace / 2f + (i * hspace) + hspace + heightPadding);
            p.lineTo((i + 1.5f) * wspace + wlong, mHeight / 2 + (i * hspace) + hspace - hlong + heightPadding);
            p.close();
            canvas.drawPath(p, mPaintLeftRight);
            int rightPosition = i;
            p.reset();
            p.moveTo(mWidth - (rightPosition + 1.5f) * wspace - wlong, mHeight / 2 + (rightPosition * hspace) - hlong + heightPadding);
            p.lineTo(mWidth - (rightPosition + 1f) * wspace - wlong, mHeight / 2 - hspace / 2f + (rightPosition * hspace) - hlong + heightPadding);
            p.lineTo(mWidth - (rightPosition + 0.5f) * wspace, mHeight / 2 + (rightPosition * hspace) + heightPadding);
            p.lineTo(mWidth - (rightPosition + 1f) * wspace, mHeight / 2 + hspace / 2f + (rightPosition * hspace) + heightPadding);
            p.close();
            canvas.drawPath(p, mPaintRightTop);
            p.reset();
            p.moveTo(mWidth - (rightPosition + 1.5f) * wspace - wlong, mHeight / 2 + (rightPosition * hspace) - hlong + heightPadding);
            p.lineTo(mWidth - (rightPosition + 1f) * wspace, mHeight / 2 + hspace / 2f + (rightPosition * hspace) + heightPadding);
            p.lineTo(mWidth - (rightPosition + 1f) * wspace, mHeight / 2 + hspace / 2f + (rightPosition * hspace) + hspace + heightPadding);
            p.lineTo(mWidth - (rightPosition + 1.5f) * wspace - wlong, mHeight / 2 + (rightPosition * hspace) + hspace - hlong + heightPadding);
            p.close();
            canvas.drawPath(p, mPaintRightLeft);
            p.reset();
            p.moveTo(mWidth - (rightPosition + 0.5f) * wspace, mHeight / 2 + (rightPosition * hspace) + heightPadding);
            p.lineTo(mWidth - (rightPosition + 1f) * wspace, mHeight / 2 + hspace / 2f + (rightPosition * hspace) + heightPadding);
            p.lineTo(mWidth - (rightPosition + 1f) * wspace, mHeight / 2 + hspace / 2f + (rightPosition * hspace) + hspace + heightPadding);
            p.lineTo(mWidth - (rightPosition + 0.5f) * wspace, mHeight / 2 + (rightPosition * hspace) + hspace + heightPadding);
            p.close();
            canvas.drawPath(p, mPaintRightRight);
        }
    }

    public void setViewColor(int color) {
        mPaintLeftTop.setColor(new Color(color));
        mPaintRightTop.setColor(new Color(color));
        int red = (color & 0xff0000) >> 16;
        int green = (color & 0x00ff00) >> 8;
        int blue = (color & 0x0000ff);
        mPaintLeftLeft.setColor(new Color(Color.rgb(
                (red - 60) > 0 ? (red - 60) : 0,
                (green - 54) > 0 ? (green - 54) : 0,
                (blue - 13) > 0 ? (blue - 13) : 0
        )));
        mPaintRightLeft.setColor(new Color(Color.rgb(
                (red - 60) > 0 ? (red - 60) : 0,
                (green - 54) > 0 ? (green - 54) : 0,
                (blue - 13) > 0 ? (blue - 13) : 0
        )));
        mPaintRightRight.setColor(new Color(Color.rgb(
                (red - 96) > 0 ? (red - 96) : 0,
                (green - 70) > 0 ? (green - 70) : 0,
                (blue - 22) > 0 ? (blue - 22) : 0
        )));
        mPaintLeftRight.setColor(new Color(Color.rgb(
                (red - 96) > 0 ? (red - 96) : 0,
                (green - 70) > 0 ? (green - 70) : 0,
                (blue - 22) > 0 ? (blue - 22) : 0
        )));
    }

    @Override
    protected void OnAnimationRepeat(Animator animation) {

    }

    @Override
    protected void OnAnimationUpdate(float value) {
        mAnimatedValue = value;
        invalidate();
    }

    @Override
    protected int OnStopAnim() {
        mAnimatedValue = 1f;
        invalidate();
        return 1;
    }

    @Override
    protected int SetAnimRepeatMode() {
        return REVERSE;
    }

    @Override
    protected void AinmIsRunning() {

    }

    @Override
    protected int SetAnimRepeatCount() {
        return Integer.MAX_VALUE;
    }

}
