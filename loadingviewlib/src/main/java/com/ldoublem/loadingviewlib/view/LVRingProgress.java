package com.ldoublem.loadingviewlib.view;

import com.ldoublem.loadingviewlib.view.base.LVBase;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Path;
import ohos.agp.render.Texture;
import ohos.agp.render.Shader;
import ohos.agp.render.LinearShader;
import ohos.agp.render.BlurDrawLooper;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

/**
 * Created by lumingmin on 16/6/27.
 */

public class LVRingProgress extends LVBase implements Component.DrawTask {

    private Paint mPaint;
    private PixelMap mBitmapBg;
    private Paint mPaintText;
    private float MaxAngle = 359f;
    private int mPadding;
    private int mWidth;
    private float mAnimatedValue = 0f;
    int ProStartColor = Color.argb(100, 0, 242, 123);
    int ProEndColor = Color.argb(100, 86, 171, 228);

    private RectFloat rectFBg = new RectFloat();
    private int Progress = 0;

    public LVRingProgress(Context context) {
        this(context, null);
    }

    public LVRingProgress(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVRingProgress(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void InitPaint() {
        mPaint = new Paint();
        mPaintText = new Paint();
        mPaintText.setAntiAlias(true);
        mPaintText.setStyle(Paint.Style.FILL_STYLE);
        mPaintText.setColor(Color.WHITE);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (getWidth() > getHeight()) {
            mWidth = getHeight();
        } else {
            mWidth = getWidth();
        }
        mPadding = mWidth / 10;
        rectFBg = new RectFloat(getWidth() / 2 - mWidth / 2 + mPadding,
                getHeight() / 2 - mWidth / 2 + mPadding,
                getWidth() / 2 + mWidth / 2 - mPadding,
                getHeight() / 2 + mWidth / 2 - mPadding);
        drawBg(canvas, mPaint);
        drawProgress(canvas, mPaint, (int) (MaxAngle / 100f * getProgress()));
    }

    private void drawBg(Canvas canvas, Paint paint) {
        canvas.drawPixelMapHolder(new PixelMapHolder(getmBitmapBg(paint)), 0,
                0, paint);
    }

    private PixelMap getmBitmapBg(Paint paint) {
        Canvas canvas;
        if (mBitmapBg == null) {
            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = new Size(getWidth(), getHeight());
            initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
            mBitmapBg = PixelMap.create(initializationOptions);
            canvas = new Canvas(new Texture(mBitmapBg));
            paint.setAntiAlias(true);
            paint.setStrokeWidth(mPadding);
            paint.setStyle(Paint.Style.STROKE_STYLE);
            Path pathBg = new Path();
            pathBg.addArc(rectFBg, 0, 360);
            paint.setBlurDrawLooper(new BlurDrawLooper(mPadding / 3, 0, mPadding / 4, new Color(Color.argb(100, 0, 0, 0))));
            canvas.drawPath(pathBg, paint);
        }
        return mBitmapBg;
    }

    private void drawProgress(Canvas canvas, Paint paint, int sweepAngle) {
        paint.reset();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(mPadding);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        Path pathProgress = new Path();
        pathProgress.addArc(rectFBg, -90, sweepAngle);
        Shader mShader = new LinearShader(new Point[]{new Point(rectFBg.left, rectFBg.top), new Point(rectFBg.left, rectFBg.bottom)},
                new float[]{0f, 1f}, new Color[]{new Color(ProStartColor), new Color(ProEndColor)}, Shader.TileMode.CLAMP_TILEMODE);
        paint.setShader(mShader, Paint.ShaderType.LINEAR_SHADER);
        paint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        paint.setStrokeJoin(Paint.Join.ROUND_JOIN);
        canvas.drawPath(pathProgress, paint);
        mPaintText.setTextSize((int) mPaint.getStrokeWidth() / 2);
        String text = (int) (sweepAngle / MaxAngle * 100) + "%";
        canvas.drawTextOnPath(mPaintText, text, pathProgress,
                (float) (Math.PI * rectFBg.getWidth() * (sweepAngle / MaxAngle) - getFontlength(mPaintText, text) * 1.5f),
                getFontHeight(mPaintText) / 3);
    }

    public int getProgress() {
        return Progress;
    }

    public void setProgress(int progress) {
        Progress = progress;
        invalidate();
    }


    public void setViewColor(int color) {
        mPaint.setColor(new Color(color));
        invalidate();
    }

    public void setTextColor(int color) {
        mPaintText.setColor(new Color(color));
        invalidate();
    }


    public void setPorBarStartColor(int color) {
        ProStartColor = color;
    }

    public void setPorBarEndColor(int color) {
        ProEndColor = color;
    }

    @Override
    protected void OnAnimationRepeat(Animator animation) {

    }

    @Override
    protected void OnAnimationUpdate(float value) {
        mAnimatedValue = value;
        setProgress((int) (mAnimatedValue * 100));
    }

    @Override
    protected int OnStopAnim() {
        mAnimatedValue = 0f;
        invalidate();
        return 1;
    }

    @Override
    protected int SetAnimRepeatMode() {
        return RESTART;
    }

    @Override
    protected void AinmIsRunning() {

    }

    @Override
    protected int SetAnimRepeatCount() {
        return 0;
    }

}
