package com.ldoublem.loadingviewlib.view;

import com.ldoublem.loadingviewlib.view.base.LVBase;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Created by lumingmin on 16/6/27.
 */

public class LVWifi extends LVBase implements Component.DrawTask {

    private float mWidth = 0f;
    private Paint mPaint;
    private int signalSize = 4;
    private float mAnimatedValue = 0.9f;

    public LVWifi(Context context) {
        this(context, null);
    }

    public LVWifi(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVWifi(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void InitPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(Color.WHITE);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (getWidth() > getHeight()) {
            mWidth = getHeight();
        } else {
            mWidth = getWidth();
        }
        canvas.translate(0, mWidth / signalSize);
        mPaint.setStrokeWidth(mWidth / signalSize / 2 / 2 / 2);
        int scale = (int) ((mAnimatedValue * signalSize - (int) (mAnimatedValue * signalSize)) * signalSize) + 1;
        RectFloat rect;
        float signalRadius = mWidth / 2 / signalSize;
        for (int i = 0; i < signalSize; i++) {
            if (i >= signalSize - scale) {
                float radius = signalRadius * i;
                rect = new RectFloat(radius, radius,
                        mWidth - radius, mWidth - radius);
                if (i < signalSize - 1) {
                    mPaint.setStyle(Paint.Style.STROKE_STYLE);
                    canvas.drawArc(rect, new Arc(-135, 90, false), mPaint);
                } else {
                    mPaint.setStyle(Paint.Style.FILL_STYLE);
                    canvas.drawArc(rect, new Arc(-135, 90, true), mPaint);
                }
            }
        }
    }

    public void setViewColor(int color) {
        mPaint.setColor(new Color(color));
        invalidate();
    }

    @Override
    protected void OnAnimationRepeat(Animator animation) {

    }

    @Override
    protected void OnAnimationUpdate(float value) {
        mAnimatedValue = value;
        invalidate();
    }

    @Override
    protected int OnStopAnim() {
        mAnimatedValue = 0.9f;
        invalidate();
        return 1;
    }

    @Override
    protected int SetAnimRepeatMode() {
        return RESTART;
    }

    @Override
    protected void AinmIsRunning() {

    }

    @Override
    protected int SetAnimRepeatCount() {
        return Integer.MAX_VALUE;
    }


}
