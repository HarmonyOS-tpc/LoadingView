package com.ldoublem.loadingviewlib.view;

import com.ldoublem.loadingviewlib.view.base.LVBase;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Created by lumingmin on 16/6/20.
 */

public class LVChromeLogo extends LVBase implements Component.DrawTask {

    private Paint mPaintRed, mPaintYellow, mPaintGreen, mPaintBulue, mPaintWhite, mPaintLine;
    private float mWidth = 0f;
    private float mPadding;
    private int startYellowColor = Color.argb(100, 253, 197, 53);
    private int startGreenColor = Color.argb(100, 27, 147, 76);
    private int startRedColor = Color.argb(100, 211, 57, 53);
    private float mAnimatedValue = 0f;

    public LVChromeLogo(Context context) {
        this(context, null);
    }

    public LVChromeLogo(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVChromeLogo(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mPadding = dip2px(1);
    }

    @Override
    protected void InitPaint() {
        mPaintRed = new Paint();
        mPaintRed.setAntiAlias(true);
        mPaintRed.setStyle(Paint.Style.FILL_STYLE);
        mPaintRed.setColor(new Color(Color.rgb(211, 57, 53)));

        mPaintYellow = new Paint();
        mPaintYellow.setAntiAlias(true);
        mPaintYellow.setStyle(Paint.Style.FILL_STYLE);
        mPaintYellow.setColor(new Color(Color.rgb(253, 197, 53)));

        mPaintGreen = new Paint();
        mPaintGreen.setAntiAlias(true);
        mPaintGreen.setStyle(Paint.Style.FILL_STYLE);
        mPaintGreen.setColor(new Color(Color.rgb(27, 147, 76)));

        mPaintBulue = new Paint();
        mPaintBulue.setAntiAlias(true);
        mPaintBulue.setStyle(Paint.Style.FILL_STYLE);
        mPaintBulue.setColor(new Color(Color.rgb(61, 117, 242)));

        mPaintWhite = new Paint();
        mPaintWhite.setAntiAlias(true);
        mPaintWhite.setStyle(Paint.Style.FILL_STYLE);
        mPaintWhite.setColor(Color.WHITE);

        mPaintLine = new Paint();
        mPaintLine.setAntiAlias(true);
        mPaintLine.setStyle(Paint.Style.FILL_STYLE);
        mPaintLine.setColor(new Color(Color.argb(30, 0, 0, 0)));
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (getWidth() > getHeight()) {
            mWidth = getHeight();
        } else {
            mWidth = getWidth();
        }
        canvas.rotate(360 * mAnimatedValue, component.getWidth() / 2, component.getHeight() / 2);
        drawSector(canvas);
        drawTriangle(canvas);
        drawCircle(canvas);
    }

    private void drawSector(Canvas canvas) { // 将圆分成三个扇形
        RectFloat rectF = new RectFloat(mPadding, mPadding, mWidth - mPadding, mWidth - mPadding);
        canvas.drawArc(rectF, new Arc(-30, 120, true), mPaintYellow);
        canvas.drawArc(rectF, new Arc(90, 120, true), mPaintGreen);
        canvas.drawArc(rectF, new Arc(210, 120, true), mPaintRed);
    }

    private void drawTriangle(Canvas canvas) { // 画三个等边三角形组成的大三角形正好是内切三角形
        MyPoint point1 = getPoint((mWidth / 2 - mPadding) / 2, 90);
        MyPoint point2 = getPoint((mWidth / 2 - mPadding), 150);
        MyPoint point3 = getPoint((mWidth / 2 - mPadding) / 2, 210);
        MyPoint point4 = getPoint((mWidth / 2 - mPadding), 270);
        MyPoint point5 = getPoint((mWidth / 2 - mPadding) / 2, 330);
        MyPoint point6 = getPoint((mWidth / 2 - mPadding), 30);
        Path pathYellow = new Path();
        pathYellow.moveTo(mWidth / 2 - point1.x, mWidth / 2 - point1.y);
        pathYellow.lineTo(mWidth / 2 - point2.x, mWidth / 2 - point2.y);
        pathYellow.lineTo(mWidth / 2 - point3.x, mWidth / 2 - point3.y);
        pathYellow.close();
        Path pathGreen = new Path();
        pathGreen.moveTo(mWidth / 2 - point3.x, mWidth / 2 - point3.y);
        pathGreen.lineTo(mWidth / 2 - point4.x, mWidth / 2 - point4.y);
        pathGreen.lineTo(mWidth / 2 - point5.x, mWidth / 2 - point5.y);
        pathGreen.close();
        Path pathRed = new Path();
        pathRed.moveTo(mWidth / 2 - point5.x, mWidth / 2 - point5.y);
        pathRed.lineTo(mWidth / 2 - point6.x, mWidth / 2 - point6.y);
        pathRed.lineTo(mWidth / 2 - point1.x, mWidth / 2 - point1.y);
        pathRed.close();
        canvas.drawPath(pathGreen, mPaintGreen);
        canvas.drawPath(pathRed, mPaintRed);
        canvas.drawPath(pathYellow, mPaintYellow);

        // 扇形交接处隐形效果
        for (int i = 0; i < Math.abs(mWidth / 2 - point2.y) / 2f; i++) {
            int fraction = 35 - i;
            if (fraction > 0) {
                mPaintLine.setColor(new Color(startYellowColor));
            } else {
                mPaintLine.setColor(new Color(Color.argb(0, 0, 0, 0)));
            }
            canvas.drawLine(new Point(mWidth / 2, point2.y + i),
                    new Point(mWidth / 2 - point2.x * 8f / 10f, mWidth / 2 - point2.y), mPaintLine);
        }
        for (int i = 0; i < Math.abs(point3.x) / 2f; i++) {
            int fraction = 35 - i;
            if (fraction > 0) {
                mPaintLine.setColor(new Color(startGreenColor));
            } else {
                mPaintLine.setColor(new Color(Color.argb(0, 0, 0, 0)));
            }
            canvas.drawLine(new Point(mWidth / 2 - point3.x - i, mWidth / 2 - point3.y),
                    new Point(mWidth / 2 - point4.x, mWidth / 2 - point4.y), mPaintLine);
        }
        for (int i = 0; i < Math.abs(mWidth / 2 - point5.x) / 2f; i++) {
            int fraction = 30 - i;
            if (fraction > 0) {
                mPaintLine.setColor(new Color(startRedColor));
            } else {
                mPaintLine.setColor(new Color(Color.argb(0, 0, 0, 0)));
            }
            canvas.drawLine(new Point(mWidth / 2 - point5.x + i, mWidth / 2 - point5.y),
                    new Point(mWidth / 2 - point6.x, mWidth / 2 - point6.y), mPaintLine);
        }
    }

    private void drawCircle(Canvas canvas) { // 画中心的圆覆盖
        canvas.drawCircle(mWidth / 2, mWidth / 2, (mWidth / 2 - mPadding) / 2, mPaintWhite);
        canvas.drawCircle(mWidth / 2, mWidth / 2, (mWidth / 2 - mPadding) / 2 / 6 * 5, mPaintBulue);
    }

    @Override
    public void startAnim() {
        startAnim(1500);
    }

    @Override
    protected void OnAnimationUpdate(float value) {
        mAnimatedValue = value;
        invalidate();
    }

    @Override
    protected void OnAnimationRepeat(Animator animation) {

    }

    @Override
    protected int OnStopAnim() {
        return 0;
    }

    @Override
    protected int SetAnimRepeatMode() {
        return RESTART;
    }

    @Override
    protected void AinmIsRunning() {

    }

    @Override
    protected int SetAnimRepeatCount() {
        return Integer.MAX_VALUE;
    }

    private class MyPoint {
        private float x;
        private float y;

        private MyPoint(float x, float y) {
            this.x = x;
            this.y = y;
        }
    }

    private MyPoint getPoint(float radius, float angle) {
        float x = (float) ((radius) * Math.cos(angle * Math.PI / 180f));
        float y = (float) ((radius) * Math.sin(angle * Math.PI / 180f));
        MyPoint p = new MyPoint(x, y);
        return p;
    }

}
