package com.ldoublem.loadingviewlib.view;

import com.ldoublem.loadingviewlib.view.base.LVBase;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;

/**
 * Created by lumingmin on 16/6/23.
 */

public class LVGears extends LVBase implements Component.DrawTask {

    private float mWidth = 0f;
    private Paint mPaint, mPaintWheelBig, mPaintWheelSmall, mPaintAxle, mPaintCenter;
    private float mPadding;
    private float mPaintCenterRadius;
    private float mWheelSmallLength, mWheelBigLength;
    private int mWheelSmallSpace = 8;
    private int mWheelBigSpace = 6;
    float mAnimatedValue = 0f;

    public LVGears(Context context) {
        this(context, null);
    }

    public LVGears(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public LVGears(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mPadding = dip2px(5);
        mWheelSmallLength = dip2px(3);
        mWheelBigLength = dip2px(2.5f);
    }

    @Override
    protected void InitPaint() {
        mPaintCenterRadius = dip2px(2.5f) / 2;
        mPaintCenter = new Paint();
        mPaintCenter.setAntiAlias(true);
        mPaintCenter.setStyle(Paint.Style.STROKE_STYLE);
        mPaintCenter.setColor(Color.WHITE);
        mPaintCenter.setStrokeWidth(dip2px(0.5f));

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(dip2px(2));

        mPaintAxle = new Paint();
        mPaintAxle.setAntiAlias(true);
        mPaintAxle.setStyle(Paint.Style.FILL_STYLE);
        mPaintAxle.setColor(Color.WHITE);
        mPaintAxle.setStrokeWidth(dip2px(2f));

        mPaintWheelBig = new Paint();
        mPaintWheelBig.setAntiAlias(true);
        mPaintWheelBig.setStyle(Paint.Style.STROKE_STYLE);
        mPaintWheelBig.setColor(Color.WHITE);
        mPaintWheelBig.setStrokeWidth(dip2px(1));

        mPaintWheelSmall = new Paint();
        mPaintWheelSmall.setAntiAlias(true);
        mPaintWheelSmall.setStyle(Paint.Style.STROKE_STYLE);
        mPaintWheelSmall.setColor(Color.WHITE);
        mPaintWheelSmall.setStrokeWidth(dip2px(0.5f));
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (getWidth() > getHeight()) {
            mWidth = getHeight();
        } else {
            mWidth = getWidth();
        }
        drawCircle(canvas);
        drawWheelBig(canvas);
        drawWheelSmall(canvas);
        drawAxleAndCenter(canvas);
    }

    private void drawCircle(Canvas canvas) {
        canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 2 - mPadding, mPaint);
        canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 4, mPaint);
    }

    private void drawAxleAndCenter(Canvas canvas) {
        for (int i = 0; i < 3; i++) {
            float x2 = (float) ((mWidth / 2.f - mPadding) * Math.cos(i * (360 / 3) * Math.PI / 180f));
            float y2 = (float) ((mWidth / 2.f - mPadding) * Math.sin(i * (360 / 3) * Math.PI / 180f));
            float x = (float) (mPaintCenterRadius * Math.cos(i * (360 / 3) * Math.PI / 180f));
            float y = (float) (mPaintCenterRadius * Math.sin(i * (360 / 3) * Math.PI / 180f));
            canvas.drawLine(new Point(mWidth / 2 - x, mWidth / 2 - y),
                    new Point(mWidth / 2 - x2, mWidth / 2 - y2), mPaintAxle);
        }
        canvas.drawCircle(mWidth / 2, mWidth / 2, mPaintCenterRadius, mPaintCenter);
    }

    private void drawWheelBig(Canvas canvas) {
        for (int i = 0; i < 360; i = i + mWheelBigSpace) {
            int angle = (int) (mAnimatedValue * mWheelBigSpace + i); // clockwise 顺时针
            float x = (float) ((mWidth / 2.f - mPadding + mWheelBigLength) * Math.cos(angle * Math.PI / 180f));
            float y = (float) ((mWidth / 2.f - mPadding + mWheelBigLength) * Math.sin(angle * Math.PI / 180f));
            float x2 = (float) ((mWidth / 2.f - mPadding) * Math.cos(angle * Math.PI / 180f));
            float y2 = (float) ((mWidth / 2.f - mPadding) * Math.sin(angle * Math.PI / 180f));
            canvas.drawLine(new Point(mWidth / 2.f - x, mWidth / 2.f - y),
                    new Point(mWidth / 2.f - x2, mWidth / 2.f - y2), mPaintWheelBig);
        }
    }

    private void drawWheelSmall(Canvas canvas) {
        for (int i = 0; i < 360; i = i + mWheelSmallSpace) {
            int angle = (int) (360 - mAnimatedValue * mWheelBigSpace + i); // anticlockwise 逆时针
            float x = (float) ((mWidth / 4.f) * Math.cos(angle * Math.PI / 180f));
            float y = (float) ((mWidth / 4.f) * Math.sin(angle * Math.PI / 180f));
            float x2 = (float) ((mWidth / 4.f + mWheelSmallLength) * Math.cos(angle * Math.PI / 180f));
            float y2 = (float) ((mWidth / 4.f + mWheelSmallLength) * Math.sin(angle * Math.PI / 180f));
            canvas.drawLine(new Point(mWidth / 2.f - x, mWidth / 2.f - y),
                    new Point(mWidth / 2.f - x2, mWidth / 2.f - y2), mPaintWheelSmall);
        }
    }

    public void setViewColor(int color) {
        mPaint.setColor(new Color(color));
        mPaintCenter.setColor(new Color(color));
        mPaintAxle.setColor(new Color(color));
        mPaintWheelBig.setColor(new Color(color));
        mPaintWheelSmall.setColor(new Color(color));
        invalidate();
    }

    @Override
    protected void OnAnimationRepeat(Animator animation) {

    }

    @Override
    protected void OnAnimationUpdate(float value) {
        mAnimatedValue = value;
        invalidate();
    }

    @Override
    protected int OnStopAnim() {
        invalidate();
        return 1;
    }

    @Override
    protected int SetAnimRepeatMode() {
        return RESTART;
    }

    @Override
    protected void AinmIsRunning() {

    }

    @Override
    protected int SetAnimRepeatCount() {
        return Integer.MAX_VALUE;
    }

}
