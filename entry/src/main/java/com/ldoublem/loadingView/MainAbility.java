package com.ldoublem.loadingView;

import com.ldoublem.loadingviewlib.LVCircularCD;
import com.ldoublem.loadingviewlib.LVLineWithText;
import com.ldoublem.loadingviewlib.view.LVCircular;
import com.ldoublem.loadingviewlib.view.LVCircularRing;
import com.ldoublem.loadingviewlib.view.LVCircularSmile;
import com.ldoublem.loadingviewlib.view.LVFinePoiStar;
import com.ldoublem.loadingviewlib.view.LVGears;
import com.ldoublem.loadingviewlib.view.LVGearsTwo;
import com.ldoublem.loadingviewlib.view.LVWifi;
import com.ldoublem.loadingviewlib.view.LVCircularJump;
import com.ldoublem.loadingviewlib.view.LVCircularZoom;
import com.ldoublem.loadingviewlib.view.LVPlayBall;
import com.ldoublem.loadingviewlib.view.LVNews;
import com.ldoublem.loadingviewlib.view.LVEatBeans;
import com.ldoublem.loadingviewlib.view.LVChromeLogo;
import com.ldoublem.loadingviewlib.view.LVRingProgress;
import com.ldoublem.loadingviewlib.view.LVBlock;
import com.ldoublem.loadingviewlib.view.LVFunnyBar;
import com.ldoublem.loadingviewlib.view.LVGhost;
import com.ldoublem.loadingviewlib.view.LVBlazeWood;
import com.ldoublem.loadingviewlib.view.LVBattery;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.Timer;
import java.util.TimerTask;

public class MainAbility extends Ability implements Component.ClickedListener {

    LVCircularCD mLVCircularCD;
    LVCircularRing mLVCircularRing;
    LVCircular mLVCircular;
    LVFinePoiStar mLVFinePoiStar;

    LVCircularSmile mLVCircularSmile;
    LVGears mLVGears;
    LVGearsTwo mLVGearsTwo;
    LVWifi mLVWifi;

    LVCircularJump mLVCircularJump;
    LVCircularZoom mLVCircularZoom;
    LVPlayBall mLVPlayBall;
    LVNews mLVNews;

    LVLineWithText mLVLineWithText;
    LVEatBeans mLVEatBeans;

    LVChromeLogo mLVChromeLogo;
    LVRingProgress mLVRingProgress;
    LVBlock mLVBlock;
    LVFunnyBar mLVFunnyBar;

    LVGhost mLVGhost;
    LVBlazeWood mLVBlazeWood;
    LVBattery mLVBattery;

    int mValueLVLineWithText = 0;
    int mValueLVNews = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        findComponentById(ResourceTable.Id_startAnimAll).setClickedListener(this);
        findComponentById(ResourceTable.Id_stopAnim).setClickedListener(this);

        mLVCircularCD = (LVCircularCD) findComponentById(ResourceTable.Id_lv_circularCD);
        mLVCircularCD.setViewColor(Color.rgb(0, 255, 0));
        mLVCircularCD.setClickedListener(this);
        mLVCircularRing = (LVCircularRing) findComponentById(ResourceTable.Id_lv_circularring);
        mLVCircularRing.setViewColor(Color.argb(100, 255, 255, 255));
        mLVCircularRing.setBarColor(Color.YELLOW.getValue());
        mLVCircularRing.setClickedListener(this);
        mLVCircular = (LVCircular) findComponentById(ResourceTable.Id_lv_circular);
        mLVCircular.setViewColor(Color.rgb(255, 99, 99));
        mLVCircular.setRoundColor(Color.rgb(255, 0, 0));
        mLVCircular.setClickedListener(this);
        mLVFinePoiStar = (LVFinePoiStar) findComponentById(ResourceTable.Id_lv_finePoiStar);
        mLVFinePoiStar.setViewColor(Color.WHITE.getValue());
        mLVFinePoiStar.setCircleColor(Color.YELLOW.getValue());
        mLVFinePoiStar.setDrawPath(true);
        mLVFinePoiStar.setClickedListener(this);

        mLVCircularSmile = (LVCircularSmile) findComponentById(ResourceTable.Id_lv_circularSmile);
        mLVCircularSmile.setViewColor(Color.rgb(144, 238, 146));
        mLVCircularSmile.setClickedListener(this);
        mLVGears = (LVGears) findComponentById(ResourceTable.Id_lv_gears);
        mLVGears.setViewColor(Color.rgb(55, 155, 233));
        mLVGears.setClickedListener(this);
        mLVGearsTwo = (LVGearsTwo) findComponentById(ResourceTable.Id_lv_gears_two);
        mLVGearsTwo.setViewColor(Color.rgb(155, 55, 233));
        mLVGearsTwo.setClickedListener(this);
        mLVWifi = (LVWifi) findComponentById(ResourceTable.Id_lv_wifi);
        mLVWifi.setViewColor(Color.BLACK.getValue());
        mLVWifi.setClickedListener(this);

        mLVCircularJump = (LVCircularJump) findComponentById(ResourceTable.Id_lv_circularJump);
        mLVCircularJump.setViewColor(Color.rgb(133, 66, 99));
        mLVCircularJump.setClickedListener(this);
        mLVCircularZoom = (LVCircularZoom) findComponentById(ResourceTable.Id_lv_circularZoom);
        mLVCircularZoom.setViewColor(Color.rgb(255, 0, 122));
        mLVCircularZoom.setClickedListener(this);
        mLVPlayBall = (LVPlayBall) findComponentById(ResourceTable.Id_lv_playball);
        mLVPlayBall.setViewColor(Color.WHITE.getValue());
        mLVPlayBall.setBallColor(Color.RED.getValue());
        mLVPlayBall.setClickedListener(this);
        mLVNews = (LVNews) findComponentById(ResourceTable.Id_lv_news);
        mLVNews.setViewColor(Color.WHITE.getValue());
        mLVNews.setClickedListener(this);

        mLVLineWithText = (LVLineWithText) findComponentById(ResourceTable.Id_lv_linetext);
        mLVLineWithText.setViewColor(Color.rgb(33, 66, 77));
        mLVLineWithText.setTextColor(Color.rgb(233, 166, 177));
        mLVLineWithText.setClickedListener(this);
        mLVEatBeans = (LVEatBeans) findComponentById(ResourceTable.Id_lv_eatBeans);
        mLVEatBeans.setViewColor(Color.WHITE.getValue());
        mLVEatBeans.setEyeColor(Color.BLUE.getValue());
        mLVEatBeans.setClickedListener(this);

        mLVChromeLogo = (LVChromeLogo) findComponentById(ResourceTable.Id_lv_chromeLogo);
        mLVChromeLogo.setClickedListener(this);
        mLVRingProgress = (LVRingProgress) findComponentById(ResourceTable.Id_lv_ringp);
        mLVRingProgress.setViewColor(Color.WHITE.getValue());
        mLVRingProgress.setTextColor(Color.BLACK.getValue());
        mLVRingProgress.setPorBarStartColor(Color.YELLOW.getValue());
        mLVRingProgress.setPorBarEndColor(Color.BLUE.getValue());
        mLVRingProgress.setClickedListener(this);
        mLVBlock = (LVBlock) findComponentById(ResourceTable.Id_lv_block);
        mLVBlock.setViewColor(Color.rgb(245, 209, 22));
        mLVBlock.setShadowColor(Color.BLACK.getValue());
        mLVBlock.setClickedListener(this);
        mLVFunnyBar = (LVFunnyBar) findComponentById(ResourceTable.Id_lv_funnybar);
        mLVFunnyBar.setViewColor(Color.rgb(234, 167, 107));
        mLVFunnyBar.setClickedListener(this);

        mLVGhost = (LVGhost) findComponentById(ResourceTable.Id_lv_ghost);
        mLVGhost.setViewColor(Color.WHITE.getValue());
        mLVGhost.setHandColor(Color.BLACK.getValue());
        mLVGhost.setClickedListener(this);
        mLVBlazeWood = (LVBlazeWood) findComponentById(ResourceTable.Id_lv_wood);
        mLVBlazeWood.setClickedListener(this);
        mLVBattery = (LVBattery) findComponentById(ResourceTable.Id_lv_battery);
        mLVBattery.setBatteryOrientation(LVBattery.BatteryOrientation.VERTICAL);
        mLVBattery.setShowNum(true);
        mLVBattery.setClickedListener(this);

    }

    @Override
    public void onClick(Component component) {
        stopAll();
        switch (component.getId()) {
            case ResourceTable.Id_startAnimAll:
                startAnimAll();
                break;
            case ResourceTable.Id_stopAnim:
                stopAll();
                break;
            case ResourceTable.Id_lv_circularCD:
                ((LVCircularCD) component).startAnim();
                break;
            case ResourceTable.Id_lv_circularring:
                ((LVCircularRing) component).startAnim();
                break;
            case ResourceTable.Id_lv_circular:
                ((LVCircular) component).startAnim();
                break;
            case ResourceTable.Id_lv_finePoiStar:
                ((LVFinePoiStar) component).setDrawPath(false);
                ((LVFinePoiStar) component).startAnim(3500);
                break;
            case ResourceTable.Id_lv_circularSmile:
                ((LVCircularSmile) component).startAnim();
                break;
            case ResourceTable.Id_lv_gears:
                ((LVGears) component).startAnim();
                break;
            case ResourceTable.Id_lv_gears_two:
                ((LVGearsTwo) component).startAnim();
                break;
            case ResourceTable.Id_lv_wifi:
                ((LVWifi) component).startAnim(9000);
                break;
            case ResourceTable.Id_lv_circularJump:
                ((LVCircularJump) component).startAnim();
                break;
            case ResourceTable.Id_lv_circularZoom:
                ((LVCircularZoom) component).startAnim();
                break;
            case ResourceTable.Id_lv_playball:
                ((LVPlayBall) component).startAnim();
                break;
            case ResourceTable.Id_lv_news:
                startLVNewsAnim();
                break;
            case ResourceTable.Id_lv_linetext:
                startLVLineWithTextAnim();
                break;
            case ResourceTable.Id_lv_eatBeans:
                ((LVEatBeans) component).startAnim(3500);
                break;
            case ResourceTable.Id_lv_chromeLogo:
                ((LVChromeLogo) component).startAnim();
                break;
            case ResourceTable.Id_lv_ringp:
                ((LVRingProgress) component).startAnim(3000);
                break;
            case ResourceTable.Id_lv_block:
                ((LVBlock) component).isShadow(false);
                ((LVBlock) component).startAnim();
                break;
            case ResourceTable.Id_lv_funnybar:
                ((LVFunnyBar) component).startAnim();
                break;
            case ResourceTable.Id_lv_ghost:
                ((LVGhost) component).startAnim();
                break;
            case ResourceTable.Id_lv_wood:
                ((LVBlazeWood) component).startAnim(500);
                break;
            case ResourceTable.Id_lv_battery:
                mLVBattery.setBatteryOrientation(LVBattery.BatteryOrientation.HORIZONTAL);
                mLVBattery.setShowNum(false);
                ((LVBattery) component).startAnim(5000);
                break;
            default:
                break;
        }
    }

    public void startAnimAll() {
        mLVCircularCD.startAnim();
        mLVCircularRing.startAnim();
        mLVCircular.startAnim();
        mLVFinePoiStar.setDrawPath(true);
        mLVFinePoiStar.startAnim(3500);
        mLVCircularSmile.startAnim(1000);
        mLVGears.startAnim();
        mLVGearsTwo.startAnim();
        mLVWifi.startAnim(9000);
        mLVCircularJump.startAnim();
        mLVCircularZoom.startAnim();
        mLVPlayBall.startAnim();
        startLVNewsAnim();
        startLVLineWithTextAnim();
        mLVEatBeans.startAnim(3500);
        mLVChromeLogo.startAnim();
        mLVRingProgress.startAnim(3000);
        mLVBlock.startAnim();
        mLVBlock.isShadow(true);
        mLVFunnyBar.startAnim();
        mLVGhost.startAnim();
        mLVBlazeWood.startAnim(500);
        mLVBattery.setBatteryOrientation(LVBattery.BatteryOrientation.VERTICAL);
        mLVBattery.setShowNum(true);
        mLVBattery.startAnim(5000);
    }

    private void stopAll() {
        mLVCircularCD.stopAnim();
        mLVCircularRing.stopAnim();
        mLVCircular.stopAnim();
        mLVFinePoiStar.stopAnim();
        mLVCircularSmile.stopAnim();
        mLVGears.stopAnim();
        mLVGearsTwo.stopAnim();
        mLVWifi.stopAnim();
        mLVCircularJump.stopAnim();
        mLVCircularZoom.stopAnim();
        mLVPlayBall.stopAnim();
        stopLVNewsAnim();
        stopLVLineWithTextAnim();
        mLVEatBeans.stopAnim();
        mLVChromeLogo.stopAnim();
        mLVRingProgress.stopAnim();
        mLVBlock.stopAnim();
        mLVFunnyBar.stopAnim();
        mLVGhost.stopAnim();
        mLVBlazeWood.stopAnim();
        mLVBattery.stopAnim();

    }

    public Timer mTimerLVLineWithText = new Timer(); // 定时器
    public Timer mTimerLVNews = new Timer(); // 定时器

    private void startLVLineWithTextAnim() {
        mValueLVLineWithText = 0;
        if (mTimerLVLineWithText != null) {
            mTimerLVLineWithText.cancel(); // 退出之前的mTimer
        }
        mTimerLVLineWithText = new Timer();
        timerTaskLVLineWithText();
    }

    private void stopLVLineWithTextAnim() {
        if (mTimerLVLineWithText != null) {
            mTimerLVLineWithText.cancel(); // 退出之前的mTimer
        }
    }

    private void startLVNewsAnim() {
        mValueLVNews = 0;
        if (mTimerLVNews != null) {
            mTimerLVNews.cancel();
        }
        mTimerLVNews = new Timer();
        timerTaskLVNews();
    }

    private void stopLVNewsAnim() {
        mLVNews.stopAnim();
        if (mTimerLVNews != null) {
            mTimerLVNews.cancel();
        }
    }

    public void timerTaskLVLineWithText() {
        mTimerLVLineWithText.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mValueLVLineWithText < 100) {
                    mValueLVLineWithText++;
                    InnerEvent innerEvent = InnerEvent.get(2);
                    innerEvent.object = mValueLVLineWithText;
                    mHandle.sendEvent(innerEvent);
                } else {
                    mTimerLVLineWithText.cancel();
                }
            }
        }, 0, 50);
    }

    public void timerTaskLVNews() {
        mTimerLVNews.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mValueLVNews < 100) {
                    mValueLVNews++;
                    InnerEvent innerEvent = InnerEvent.get(1);
                    innerEvent.object = mValueLVNews;
                    mHandle.sendEvent(innerEvent);
                } else {
                    mTimerLVNews.cancel();
                }
            }
        }, 0, 10);
    }

    private EventHandler mHandle = new EventHandler(EventRunner.getMainEventRunner()) {
        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            switch (event.eventId) {
                case 1:
                    mLVNews.setValue((int) event.object);
                    break;
                case 2:
                    mLVLineWithText.setValue((int) event.object);
                    break;
            }
        }
    };

}
